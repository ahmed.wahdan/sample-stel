#!/bin/bash

set -e
# You can access inputs declared in step.yml as environment variables
echo "Custom Input value: ${MY_CUSTOM_INPUT}"

# Your custom script logic here
echo "Running my custom script..."

# => DONE